<?php

namespace App\Tests\Fonctionnel;

use App\Entity\Etat;
use App\Entity\Task;
use App\Repository\EtatRepository;
use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class TaskTest extends WebTestCase
{
    public function testTaskPage(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/');

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        $this->assertSelectorExists('h1');
        $this->assertSelectorTextContains('h1', 'Gestion des tâches');
    }

    public function testPagination(): void
    {
        $client = static::createClient();
        $crawler = $client->request(Request::METHOD_GET, '/');
   
        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        $tasks = $crawler->filter('div.card');
        $this->assertEquals(5, count($tasks));
       
        $link = $crawler->selectLink('2')->extract(['href'])[0];
        $crawler = $client->request(Request::METHOD_GET, $link);

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        $tasks = $crawler->filter('div.card');
       
        $this->assertGreaterThanOrEqual(1, count($tasks));
    }


    /**
     * Tester si le filtre de la page Index retourne les résultat souhaité
     */
    
    public function testFilterIndex(): void
    {
        $client = static::createClient();

        /** @var UrlGeneratorInterface */
        $urlGeneratorInterface = $client->getContainer()->get('router');

        /** @var EntityManagerInterface */
        $entityManager = $client->getContainer()->get('doctrine.orm.entity_manager');

        /** @var TaskRepository */
        $taskRepository = $entityManager->getRepository(Task::class);

        /** @var EtatRepository */
        $etatRepository = $entityManager->getRepository(Etat::class);

        /** @var Task */
        $task = $taskRepository->findOneBy([]);

       

        /** @var Etat */
        $etat = $etatRepository->findOneBy([]);

        $crawler = $client->request(
            Request::METHOD_GET,
            $urlGeneratorInterface->generate('app_task')
        );

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Récupérer les 3 premier caracteres du titre de la tache et de l'etat pour tester le resultat
        $searchs = [
            substr($task->getTitre(), 0, 3), // 'Tit'
            substr($etat->getLibelle(), 0, 3) // 'En '
        ];

      

        foreach ($searchs as $search) {
            $form = $crawler->filter('form[name=search]')->form([
                'search[mot]' => $search,
                'search[etat]' => 2  // Tache Terminée
            ]);


      
            $crawler = $client->submit($form);

            $this->assertResponseIsSuccessful();
            $this->assertResponseStatusCodeSame(Response::HTTP_OK);
            $this->assertRouteSame('app_task');

            $nbTasks = count($crawler->filter('div.card'));
            $taches = $crawler->filter('div.card');
            $count = 0;
      
            foreach ($taches as $index => $task) {
                $titre = $crawler->filter("div.card h6")->getNode($index);
                if (
                    str_contains($titre->textContent, $search)
                ) {
                    $taskEtat = $crawler->filter('div.card div.badges')->getNode($index)->childNodes;

                    for ($i = 1; $i < $taskEtat->count(); $i++) {
                        $tasketat = $taskEtat->item($i);
                        $name = trim($tasketat->textContent);

                        if ($name === $etat->getName()) {
                            $count++;
                        }
                    }
                }
            }

            $this->assertEquals($nbTasks, $count);
        }

        
    }



}
