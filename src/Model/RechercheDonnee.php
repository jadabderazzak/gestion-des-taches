<?php

namespace App\Model;

use App\Entity\Etat;


class RechercheDonnee {

    /** @var int */
    public $page = 1;

     /** @var string */
     public $mot = '';

     /** @var Etat|null */
     public $etat ;
}