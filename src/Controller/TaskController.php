<?php

namespace App\Controller;

use DateTime;
use App\Entity\Task;
use App\Form\TaskType;
use App\Form\SearchType;
use App\Model\RechercheDonnee;
use App\Repository\EtatRepository;
use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TaskController extends AbstractController
{
    #[Route('/', name: 'app_task')]
    public function index(TaskRepository $repoTask, PaginatorInterface $paginateur, Request $request, EtatRepository $repoEtat): Response
    {

        $rechercheDonnee = new RechercheDonnee();
        $form = $this->createForm(SearchType::class,$rechercheDonnee);

     
      
       
       
        $tasks = $paginateur->paginate(
            $repoTask->queryPagination(),
            $request->query->get('page', 1),
            5
        );

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            // Récuperation de l'id de l'etat depuis le request
            $etatId = $request->get('search')['etat'];
            // Recuperer l'entité Etat
            $etat = $etatId !== "" ? $repoEtat->findOneBy(['id'=> $etatId]) : null;

            $rechercheDonnee->page = $request->query->getInt('page', 1);
           // Paginer le resultat de findByTitle 
            $tasks = $paginateur->paginate(
                $repoTask->findByTitle($rechercheDonnee, $etat),
                $request->query->get('page', 1),
                5
            );
            return $this->render('task/index.html.twig', [
                'tasks' => $tasks,
                'form' => $form->createView()
            ]);

        }
        return $this->render('task/index.html.twig', [
            'tasks' => $tasks,
            'form' => $form->createView()
        ]);
    }

    #[Route('/task/new', name: 'app_task_new')]
    public function newTask(Request $request, EntityManagerInterface $manager, EtatRepository $repoEtat): Response
    {
       
          $task = new Task();
    
        $form= $this->createForm(TaskType::class, $task);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
                // Récuperer l'etat dont l'I est égale à 1
                $etat = $repoEtat->findOneBy([
                    'id' => 1
                ]);
                // Ajouter la date de création et l'etat en cours
                $task->setEtat($etat);
       

            $manager->persist($task);
            $manager->flush();
            
            $this->addFlash('success',"Tâche ajoutée avec succès.");
            // Faire une redirection vers la liste des tâches 
            return $this->redirectToRoute('app_task');
        }

        
        return $this->render('task/new.html.twig', [
            'formulaire' => $form->createView()
        ]);
    }


    #[Route('/task/{id}/edit', name: 'app_task_edit')]
    public function editTask(Task $task, Request $request, EntityManagerInterface $manager, EtatRepository $repoEtat): Response
    {
       
         
     

        $form= $this->createForm(TaskType::class, $task);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
               
            $manager->flush();

            $this->addFlash('success',"Tâche modifiée avec succès.");
            // Faire une redirection vers la liste des tâches 
            return $this->redirectToRoute('app_task');
        }

        
        return $this->render('task/update.html.twig', [
            'formulaire' => $form->createView(),
            
        ]);
    }

    #[Route('/task/status/finish/{id}', name: 'app_finish_Task_Status')]
    public function finishTaskStatus(Task $task, Request $request, EntityManagerInterface $manager, EtatRepository $repoEtat): Response
    {
       $etat = $repoEtat->findOneBy([
        'id' => 2
       ]);
       $task->setEtat($etat);
       $manager->flush();

        return $this->json($etat->getLibelle(), 200);
    }

    #[Route('/task/status/cancel/{id}', name: 'app_change_cancel_Status')]
    public function cancelTaskStatus(Task $task, Request $request, EntityManagerInterface $manager, EtatRepository $repoEtat): Response
    {
       
        $etat = $repoEtat->findOneBy([
            'id' => 3
           ]);
           $task->setEtat($etat);
           $manager->flush();
        

        return $this->json($etat->getLibelle(), 200);
    }



    #[Route('/tables', name: 'app_task_tables')]
    public function datatables(TaskRepository $repoTask): Response
    {

        
          $tasks = $repoTask->findAll();
        return $this->render('task/datatables.html.twig', [
            'tasks' => $tasks,
          
        ]);
    }
    
}
