<?php

namespace App\Form;

use App\Entity\Etat;
use App\Model\RechercheDonnee;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('mot', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Recherche par Titre ...'
                ]
            ])
            ->add('etat', EntityType::class, [
                'required' => false,
                'class' => Etat::class,
                'placeholder' => 'Tous les etats',
                'choice_label' => 'libelle', 
          
            
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => RechercheDonnee::class,
            'method' => 'GET',
            'csrf_protection' => false
            // Configure your form options here
        ]);
    }
}
