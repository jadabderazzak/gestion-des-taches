<?php

namespace App\Form;

use App\Entity\Task;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class TaskType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('titre', TextType::class,[

                'attr' => [
                    'placeholder' => 'Veuillez introduire un titre',
                    'class' => 'form-control-lg'
                ],
                'required' => false
            ])
            ->add('description', TextareaType::class,[
                'attr' => [
                    'placeholder' => 'Veuillez introduire une description',
                    'class' => 'form-control-lg'
                ],
                'required' => false
            ])
            ->add('Enregistrer', SubmitType::class,[
                'label' => 'Enregistrer',
                'attr' => [
                    'class' => 'btn btn-success',
                    'style' => 'font-size: 18px',
                ]
                
            ])
       
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Task::class,
        ]);
    }
}
