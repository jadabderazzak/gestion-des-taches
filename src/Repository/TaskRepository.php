<?php

namespace App\Repository;

use App\Entity\Etat;
use App\Entity\Task;
use App\Model\RechercheDonnee;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @extends ServiceEntityRepository<Task>
 *
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Task::class);
    }

    public function save(Task $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Task $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

   
    public function queryPagination()
    {
        return $this->createQueryBuilder('t')
            
            ->orderBy('t.id', 'DESC')
            ->getQuery()
        
        ;
    }

    public function findByTitle(RechercheDonnee $rechDonnee, Etat $etat = null)
    {
      

        $tasks =  $this->createQueryBuilder('t')
                        -> andWhere('t.titre LIKE :mot')
                        ->setParameter('mot',"%{$rechDonnee->mot}%");
            
                        if($etat !== null )
                        {
                      $tasks   -> andWhere('t.etat = :etat')
                        ->setParameter('etat',$etat);
                        }
                        $tasks  ->addOrderBy('t.creeLe', 'DESC');

        return  $tasks->getQuery();

       

    }

}
