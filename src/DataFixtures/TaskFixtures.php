<?php

namespace App\DataFixtures;

use DateTime;
use App\Entity\Etat;
use App\Entity\Task;
use App\Repository\EtatRepository;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class TaskFixtures extends Fixture 
{
   
   

    public function load( ObjectManager $manager): void
    {
        
        
        $etat = $manager->getRepository(Etat::class)->find(1);
       
        

        for ($i = 0; $i < 50; $i++) {
            $task = new Task();
            $task->setTitre("Titre ".$i)
                  ->setDescription("Description ".$i)
                  ->setEtat($etat);
            $manager->persist($task);

        }

        $manager->flush();
    }

   
}
